<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    protected $fillable = [
        'produto',
        'quantidade',     
        'motivo',
        'numero_nota',
        'funcionario'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
