<?php

namespace App\Http\Controllers\Administracao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use App\RequisicaoSaidas;

class AdministracaoController extends Controller
{
    public function index(RequisicaoSaidas $saidas){

        $user = auth()->user();

        $saidas = DB::table('requisicao_saidas')->where('lida', 0)->get();
        $numero_saidas = count($saidas);

        return view('administracao.home.index', compact('numero_saidas','user'));
    }
}
