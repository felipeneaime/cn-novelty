<?php

namespace App\Http\Controllers\Administracao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function profile()
    {
        return view('administracao.profile.edit-profile', compact('user'));
    }

    public function profileUpdate(Request $request)
    {
        $user = auth()->user();
        $data = $request->all();

        if($data['password'] != null)
        {
            $data['password'] = bcrypt($data['password']);
        }else{
            unset($data['password']);
        }
        
        $data['image'] = $user->image;

        if($request->hasFile('image') && $request->file('image')->isValid())
        {
            if($user->image)
            {
                $name = $user->image;
            }else{
                $name = $user->id.kebab_case($user->name);
                $extenstion = $request->image->extension();
                $name = "{$name}.{$extenstion}";
            }
            $nameFile = $name;
            

            $data['image'] = $nameFile;

            //dd($data['image']);

            $upload = $request->image->storeAs('users', $nameFile);

            if(!$upload)
            {
                return redirect()
                            ->back()
                            ->with('error');
            }
            
        }

        $update = $user->update($data);
        
        if($update)
        {
            return redirect()
                    ->route('profile')
                    ->with('success', 'Dados Atualizados com Sucesso');
        }else{
            return redirect()
                        ->back()
                        ->with('error', 'Error ao atualizar dados!');
        }
            
        
    }
}
