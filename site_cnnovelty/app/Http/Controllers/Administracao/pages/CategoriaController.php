<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CategoriaProdutos;

use Gate;

class CategoriaController extends Controller
{
    public function index(CategoriaProdutos $categoriaproduto)
    {
        $categoriaprodutos = $categoriaproduto->all();
        return view('administracao.pages.cadastro.categoria-produtos', compact('categoriaprodutos'));
    }

    public function create()
    {
        return view('administracao.pages.cadastro.createcategoria-produtos');
    }

    public function save(Request $request)
    {
        //dd($request->all());
        $categoriaproduto = [
            'name' => $request->name,
        ];

        $save = CategoriaProdutos::insert($categoriaproduto);
        if($save){
            return redirect()->route('list.categoria-produto');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function edit($idCategoriaProduto)
    {
        $categoriaproduto = CategoriaProdutos::find($idCategoriaProduto);

        //$this->authorize('edit-portifolio', $portifolio);

        if( Gate::denies ('edit', $categoriaproduto))
            abort('403', "Não Autorizado");

        return view('administracao.pages.cadastro.editcategoria-produtos', compact('categoriaproduto'));
    }

    public function update(Request $request, $id)
    {
        $categoriaproduto = [
            'name' => $request->name,
        ];

        $update = CategoriaProdutos::find($id)->update($categoriaproduto);

        if($update){
            //dd($request->all());
            return redirect()->route('list.categoria-produto')->with('sucess', 'Categoria Atualizado com sucesso');
        }else{
            return redirect()->back()->withInput();
        }
        dd($request->all());
    }
}
