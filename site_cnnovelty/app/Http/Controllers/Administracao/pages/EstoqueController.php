<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\EntradaEstoque;
use App\SaidaEstoque;
use App\Produtos;
use App\CategoriaProdutos;
use App\RequisicaoSaidas;

use App\User;

class EstoqueController extends Controller
{
    public function entrada(Produtos $produto)
    {
        $produtos = $produto->all();
        return view('administracao.pages.estoque.entrada', compact('produtos'));
    }

    public function saida(Produtos $produto)
    {
        $produtos = $produto->all();
        return view('administracao.pages.estoque.saida', compact('produtos'));
    }

    public function saveEntrada(Request $request)
    {
        $user = auth()->user();
        //dd($request->all());
        $produto = [
            'produto' => $request->produto,
            'quantidade' => $request->quantidade,
            'motivo' =>$request->motivo,
            'numero_nota' => $request->numero_nota,
            'funcionario' => $user->name
        ];
        $id = explode(";",$produto['produto']);
        $idProduto = $id[0];
        $select_produto = Produtos::find($idProduto);

        $quantidade_atual = $select_produto->quantidade_estoque;

        $quantidade_entrada = $quantidade_atual+$produto['quantidade'];


        //dd($select_produto);

        $update = Produtos::find($idProduto)->update(['quantidade_estoque' => $quantidade_entrada]);

        $save = EntradaEstoque::insert($produto);
        if($save){
            return redirect()->route('listStatus.produto');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function saveSaida(Request $request)
    {
        $user = auth()->user();
        //dd($request->all());
        $produto = [
            'produto' => $request->produto,
            'quantidade' => $request->quantidade,
            'motivo' =>$request->motivo,
            'numero_requisicao' => $request->numero_requisicao,
            'funcionario' => $user->name
        ];
        $id = explode(";",$produto['produto']);
        $idProduto = $id[0];
        $select_produto = Produtos::find($idProduto);

        $quantidade_atual = $select_produto->quantidade_estoque;

        $quantidade_saida = $quantidade_atual-$produto['quantidade'];


        //dd($select_produto);

        $update = Produtos::find($idProduto)->update(['quantidade_estoque' => $quantidade_saida]);

        $save = SaidaEstoque::insert($produto);
        if($save){
            return redirect()->route('listStatus.produto');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function listStatus(Produtos $produto, CategoriaProdutos $categoriaproduto)
    {
        $produtos = $produto->all();
        $categoriaprodutos = $categoriaproduto->all();
        return view('administracao.pages.estoque.list-status', compact('produtos','categoriaprodutos'));
    }

    public function statusProduto($idProduto, CategoriaProdutos $categoriaproduto){
        $produto = Produtos::find($idProduto);
        $categoriaprodutos = $categoriaproduto->all();

        // dd($idProduto);

        $movimentacoesEntradas = DB::table('entrada_estoques')->where('produto', $idProduto)->get();

        $movimentacoesSaidas = DB::table('saida_estoques')->where('produto', $idProduto)->get();

        // dd($movimentacoes);

        return view('administracao.pages.estoque.status', compact('produto', 'categoriaprodutos','movimentacoesEntradas', 'movimentacoesSaidas'));
        // dd($select_produto);

    }

    public function requisicoesSaidas(RequisicaoSaidas $saida)
    {
        $saidas = $saida->all();
        return view('administracao.pages.estoque.requisicao-saidas', compact('saidas'));
    }

}
