<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Expertise;

class ExpertiseController extends Controller
{
    public function index(Expertise $expertise)
    {
        $expertises = $expertise->all();
        return view('administracao.pages.expertise', compact('expertises'));
    }

    public function editExpertise($idExpertise)
    {
        $expertise = Expertise::find($idExpertise);
        return view('administracao.pages.editexpertise', compact('expertise'));
    }

    public function updateExpertise(Request $request, $id)
    {
        //$expertise = $request->titulo . $request->descricao;
        $expertise = [
            'titulo' => $request->titulo,
            'descricao' => $request->descricao,
            'icone' => $request->icone
        ];

        $update = Expertise::find($id)->update($expertise);
        if($update)
            return redirect()->route('list.expertise')->with('sucess', 'Expertise Atualizada com sucesso!');
        else
            return redirect()->back()->withInput();

    }

}
