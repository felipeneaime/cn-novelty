<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Orcamento;
use App\Produtos;
use App\Http\Requests\OrcamentoValidationFormRequest;

use PDF;

class OrcamentoController extends Controller
{
    private $produto;
    public function index(Orcamento $orcamento)
    {
        $orcamentos = $orcamento->all();
        return view('administracao.pages.vendas.orcamentos', compact('orcamentos'));
    }

    public function create(Produtos $produto)
    {
        $produtos = $produto->all();
        return view('administracao.pages.vendas.create-orcamentos', compact('produtos'));
    }

    public function teste()
    {
        return view('administracao.pages.vendas');
    }
    public function save(OrcamentoValidationFormRequest $request)
    {

        //Captura o usuario Logado
        $user = auth()->user();

        //Inicialização de variaveis
        $total_parcial = 0;
        $total = 0;
        $nomes_produtos = "";
        $quantidade_geral = "";
        $lista_valores = "";
        $totais_parciais_gerais = "";

        for ($i = 1; $i < count($request->produto); $i++) 
        {

            $produtos = [
                'descricao' => $request->produto,
                'qtd' => $request->qtd,
                'valor' => $request->valor,
            ];
           

            if($produtos['descricao'][$i] != null){

                //Separa os nomes dos produtos concatenado pelo javascript da pagina
                $descricao = $produtos['descricao'][$i];
                $descricao = explode(":",$descricao);
                $nome_produto = $descricao[1];

                $quant = $produtos['qtd'][$i];

                $valor = $produtos['valor'][$i];

                if($quant != 0) {
                    $total_parcial = $quant*$valor;
                    
                    //echo $total_parcial."<br>";
                    //$total  += $total_parcial[$i];
                    
                    //echo $total_parcial."<br>";
                    //echo $nome_produto." || ".$quant[$i]." X  ".$valor[$i]." = ".$total_parcial."<br>";
                }

                //Concatena todos os nomes dos produtos
                $nomes_produtos = $nomes_produtos.":".$nome_produto;

                //Concatena todas as quantidade de produtos
                $quantidade_geral = $quantidade_geral.":".$quant;
                
                // Concatena os valores de cada produto

                $lista_valores = $lista_valores.":".$produtos['valor'][$i];
               
                //Concatena todos os totais parciais
                $totais_parciais_gerais = $totais_parciais_gerais.":".$total_parcial;
            }
            
            
            //Atribui soma a cada Loop no valor Total
            $total +=$total_parcial;
            //dd($total);
            
        }

        $data_validade = "30-05-2018";

        $orcamento = [
            'nome_cliente' => $request->nomecliente,
            'logradouro' => $request->logradouro,
            'cidade' => $request->cidade,
            'estado' => $request->estado,
            'nomes_produtos' => $nomes_produtos,
            'quantidades_produtos' => $quantidade_geral,
            'valores_produtos' => $lista_valores,
            'totais_parciais' => $totais_parciais_gerais,
            'total' => $total,
            'data_validade' => $data_validade,
            'nome_funcionario' => $user->name,
        ];

        
        $save = Orcamento::insert($orcamento);

        if($save){

            return redirect()
                ->route('list.orcamento')
                ->with('success', 'Onçarmento Incluido com sucesso');
 
        }else{
            return redirect()->back()->with('error', $save['message']);
        }

         // dd($orcamento['nomes_produtos']);
        //$pdf = PDF::loadView('administracao/pages/orcamentos/pdf.invoice', $orcamento);
        //return $pdf->stream('invoice.pdf');
        //echo $total;
        
        //echo "<br><br> Vendedor:".$user->name;
    }

    public function editOrcamento($idOrcamento)
    {
        $orcamento = Orcamento::find($idOrcamento);

        if( Gate::denies ('edit', $produto))
            abort('403', "Não Autorizado");

        return view('administracao.pages.vendas.edit-orcamentos', compact('orcamento'));
    }

    public function viewOrcamentos($idOrcamento){
        $orcamentos = Orcamento::find($idOrcamento);

        $produtos = explode(":",$orcamentos->nomes_produtos);

        $quantidades = explode(":",$orcamentos->quantidades_produtos);

        $valores = explode(":",$orcamentos->valores_produtos);

        $totais_parciais = explode(":", $orcamentos->totais_parciais);

        return view('administracao.pages.vendas.view-orcamentos', compact('orcamentos','produtos','quantidades','valores', 'totais_parciais'));
        //$this->authorize('edit-portifolio', $portifolio);

    }

    public function pdfOrcamentos($idOrcamento){
        $orcamento = Orcamento::find($idOrcamento);
        $pdf = PDF::loadView('administracao/pages/vendas/pdf.invoice', $orcamento);
        return $pdf->stream('invoice.pdf');
    }
}