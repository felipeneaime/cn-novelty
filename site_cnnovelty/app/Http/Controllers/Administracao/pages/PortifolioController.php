<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Portifolio;
use Gate;

class PortifolioController extends Controller
{
    public function index(Portifolio $portifolio)
    {
        $portifolios = $portifolio->all();

        //$portifolios = $portifolio->where('user_id', auth()->user()->id)->get();

        return view('administracao.pages.portifolio', compact('portifolios'));
    }

    public function create()
    {
        return view('administracao.pages.createportifolio');
    }

    public function save(Request $request)
    {

        //dd($request->all());
        $portifolio = [
            'titulo' => $request->titulo,
            'min_descricao' => $request->min_descricao,
            'empresa' => $request->empresa,
            'sobre_empresa' => $request->sobre_empresa,
            'imagem' => $request->imagem,
            'descricao' => $request->descricao
        ];

        $save = Portifolio::insert($portifolio);
        if($save){
            return redirect()->route('list.portifolio');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function edit($idPortifolio)
    {
        $portifolio = Portifolio::find($idPortifolio);

        //$this->authorize('edit-portifolio', $portifolio);

        if( Gate::denies ('edit', $portifolio))
            abort('403', "Não Autorizado");

        return view('administracao.pages.editportifolio', compact('portifolio'));
    }

    public function update(Request $request, $id)
    {
        $nameFile = null;

        for ($i = 0; $i < count($request->galeria_port); $i++) 
        {

            if ($request->hasFile('galeria_port')) {
                
               // Define um aleatório para o arquivo baseado no timestamps atual
               $name = "img-".$i;
    
               // Recupera a extensão do arquivo
               $extension = $request->galeria_port[$i]->extension();

               // Define finalment o nome
               $nameFile = "{$name}"."."."{$extension}";
    
               $caminhoGaleria = "img-portifolio/".$id."/galeria";
        
               // Faz o upload:
               $upload = $request->galeria_port[$i]->storeAs($caminhoGaleria, $nameFile);
               // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao
        
               // Verifica se NÃO deu certo o upload (Redireciona de volta)
               if ( !$upload )
                   return redirect()
                               ->back()
                               ->with('error', 'Falha ao fazer upload')
                               ->withInput();
        
            }

            if($i == 0)
            {
                $galeria_port = $nameFile;
            }

            if($i != 0)
            {
                $galeria_port = $galeria_port.";".$nameFile;
            }
            
        }

        // Se informou o arquivo, retorna um boolean
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            
           // Define um aleatório para o arquivo baseado no timestamps atual
           $name = "port-capa-".$id;

           // Recupera a extensão do arquivo
           $extension = $request->image->extension();
    
           // Define finalment o nome
           $nameFile = "{$name}.{$extension}";

           $caminhoCapa = "img-portifolio/".$id."/capa";
    
           // Faz o upload:
           $upload = $request->image->storeAs($caminhoCapa, $nameFile);
           // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao
    
           // Verifica se NÃO deu certo o upload (Redireciona de volta)
           if ( !$upload )
               return redirect()
                           ->back()
                           ->with('error', 'Falha ao fazer upload')
                           ->withInput();
    
        }
        //dd($name);

        $user = auth()->user();

        if($request->image != null)
        {
            $portifolio =[
                'titulo' => $request->titulo,
                'min_descricao' => $request->min_descricao,
                'empresa' => $request->empresa,
                'sobre_empresa' => $request->sobre_empresa,
                'imagem' => $nameFile,
                'galeria_port' => $galeria_port,
                'descricao' => $request->descricao
            ];
        }

        if($request->image == null)
        {
            $portifolio =[
                'titulo' => $request->titulo,
                'min_descricao' => $request->min_descricao,
                'empresa' => $request->empresa,
                'sobre_empresa' => $request->sobre_empresa,
                //'imagem' => $nameFile,
                'galeria_port' => $galeria_port,
                'descricao' => $request->descricao
            ];
        }

        // dd($portifolio);
    
        $update = Portifolio::find($id)->update($portifolio);

        if($update){
            return redirect()->route('list.portifolio')->with('sucess', 'Portifolio Atualizado com sucesso');
        }else{
            return redirect()->back()->withInput();
        }
    }

}
