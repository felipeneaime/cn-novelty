<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Produtos;
use App\CategoriaProdutos;

use Gate;

class ProdutoController extends Controller
{
    public function index(Produtos $produto, CategoriaProdutos $categoriaproduto)
    {
        $produtos = $produto->all();
        $categoriaprodutos = $categoriaproduto->all();
        return view('administracao.pages.cadastro.produtos', compact('produtos','categoriaprodutos'));
    }

    public function create(CategoriaProdutos $categoriaproduto)
    {
        $categoriaprodutos = $categoriaproduto->all();
        return view('administracao.pages.cadastro.createprodutos', compact('categoriaprodutos'));
    }

    public function save(Request $request)
    {
        //dd($request->all());
        $produto = [
            'name' => $request->name,
            'valor' => $request->valor,
            'categoria_id' =>$request->categoria,
            'quantidade_estoque' => 0
        ];

        $save = Produtos::insert($produto);
        if($save){
            return redirect()->route('list.produto');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function edit($idProduto, CategoriaProdutos $categoriaproduto)
    {
        $produto = Produtos::find($idProduto);
        $categoriaprodutos = $categoriaproduto->all();

        //$this->authorize('edit-portifolio', $portifolio);

        if( Gate::denies ('edit', $produto))
            abort('403', "Não Autorizado");

        return view('administracao.pages.cadastro.edit-produtos', compact('produto', 'categoriaprodutos'));
    }

    public function update(Request $request, $id)
    {
        $produto = [
            'name' => $request->name,
            'valor' => $request->valor,
            'categoria_id' => $request->categoria_id,
        ];

        $update = Produtos::find($id)->update($produto);

        if($update){
            //dd($request->all());
            return redirect()->route('list.produto')->with('sucess', 'Categoria Atualizado com sucesso');
        }else{
            return redirect()->back()->withInput();
        }
        dd($request->all());
    }
}
