<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\RequisicaoSaidas;
use App\Produtos;
use PDF;

class RequisicaoSaidasController extends Controller
{
    public function index(RequisicaoSaidas $saida)
    {
        $user = auth()->user();
        $saidas = DB::table('requisicao_saidas')->where('nome_funcionario', $user->name)->get();
        return view('administracao.pages.vendas.requisicao-saidas', compact('saidas'));
    }

    public function create(Produtos $produto)
    {
        $produtos = $produto->all();
        return view('administracao.pages.vendas.create-requisicao-saida', compact('produtos'));
    }

    public function save(Request $request)
    {

        //Captura o usuario Logado
        $user = auth()->user();

        $nomes_produtos = "";
        $quantidade_geral = "";

        for ($i = 1; $i < count($request->produto); $i++) 
        {

            $produtos = [
                'descricao' => $request->produto,
                'qtd' => $request->qtd,
            ];
           

            if($produtos['descricao'][$i] != null){

                //Separa os nomes dos produtos concatenado pelo javascript da pagina
                $descricao = $produtos['descricao'][$i];
                $descricao = explode(":",$descricao);
                $nome_produto = $descricao[1];
                $quant = $produtos['qtd'][$i];

                //Concatena todos os nomes dos produtos
                $nomes_produtos = $nomes_produtos.":".$nome_produto;

                //Concatena todas as quantidade de produtos
                $quantidade_geral = $quantidade_geral.":".$quant;
                
            }
            
        }

        $requisicao_saida = [
            'numero_nota_fiscal' => $request->numero_nota_fiscal,
            'nomes_produtos' => $nomes_produtos,
            'quantidades_produtos' => $quantidade_geral,
            'lida' => 0,
            'atendida' => 0,
            'nome_funcionario' => $user->name,
        ];

        $save = RequisicaoSaidas::insert($requisicao_saida);
        if($save){
            return redirect()->route('list.saidas');
        }else{
            return redirect()->back()->withInput();
        }

         // dd($orcamento['nomes_produtos']);
        //$pdf = PDF::loadView('administracao/pages/orcamentos/pdf.invoice', $orcamento);
        //return $pdf->stream('invoice.pdf');
        //echo $total;
        
        //echo "<br><br> Vendedor:".$user->name;
    }
    public function viewRequisicaoSaida($idRequisicao){
        $requisicao = RequisicaoSaidas::find($idRequisicao);

        $produtos = explode(":",$requisicao->nomes_produtos);

        $quantidades = explode(":",$requisicao->quantidades_produtos);

        DB::table('requisicao_saidas')
        ->where('id', $idRequisicao)
        ->update(['lida' => 1]);

        return view('administracao.pages.vendas.view-requisicao-saida', compact('requisicao','produtos','quantidades','valores', 'totais_parciais'));
        //$this->authorize('edit-portifolio', $portifolio);

    }
    public function pdfRequisicao($idRequisicao){
        $requisicao = RequisicaoSaidas::find($idRequisicao);
        $pdf = PDF::loadView('administracao/pages/vendas/pdf.requisicao', $requisicao);
        return $pdf->stream('invoice.pdf');
    }
    public function requisicaoLida($idRequisicao){
        $requisicao = RequisicaoSaidas::find($idRequisicao);
        $pdf = PDF::loadView('administracao/pages/vendas/pdf.requisicao', $requisicao);
        return $pdf->stream('invoice.pdf');
    }
}
