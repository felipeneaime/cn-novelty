<?php

namespace App\Http\Controllers\Administracao\Pages;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Servico;

class ServicoController extends Controller
{
    public function index(Servico $servicos)
    {
        $servicos = $servicos->all();
        return view('administracao.pages.servicos', compact('servicos'));
    }

    public function create()
    {
        return view('administracao.pages.createservicos');
    }

    public function save(Request $request)
    {
        //dd($request->all());
        $servico = [
            'titulo' => $request->titulo,
            'min_descricao' => $request->min_descricao,
            'imagem' => $request->imagem,
            'descricao' => $request->descricao,
            'categoria' => $request->categoria
        ];

        $save = Servico::insert($servico);
        if($save){
            return redirect()->route('list.servicos');
        }else{
            return redirect()->back()->withInput();
        }
    }

    public function edit($idServico)
    {
        $servico = Servico::find($idServico);
        return view('administracao.pages.editservico', compact('servico'));
    }

    public function update(Request $request, $id)
    {
        $servico =[
            'titulo' => $request->titulo,
            'min_descricao' => $request->min_descricao,
            'imagem' => $request->imagem,
            'descricao' => $request->descricao,
            'categoria' => $request->categoria
        ];

        $update = Servico::find($id)->update($servico);

        if($update){
            return redirect()->route('list.servicos')->with('sucess', 'Portifolio Atualizado com sucesso');
        }else{
            return redirect()->back()->withInput();
        }
        //dd($request->all());
    }
}
