<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Expertise;
use App\Portifolio;
use App\Servico;

class SiteController extends Controller
{
    //public function index(){
        //return view('site.home.index');
    //}

    public function index(Expertise $expertise)
    {
        $expertises = $expertise->all();
        return view('site.home.index', compact('expertises'));
    }

    public function portifolio(Portifolio $portifolio)
    {
        $portifolios = $portifolio->all();
        return view('site.portifolio.index', compact('portifolios'));
    }

    public function detalhePortifolio($idPortifolio)
    {
        $portifolio = Portifolio::find($idPortifolio);
        $galeria_imagens = $portifolio->galeria_port;

        
        $imgs = explode(";", $galeria_imagens);
        $img_inicial= $imgs[0];
        // dd($imgs[0]);

        //dd($galeria_imagens);
        return view('site.portifolio.detalhe', compact('portifolio','img_inicial','imgs'));
    }

    public function servico(Servico $servico)
    {
        $servicos = $servico->all();
        return view('site.servico.index', compact('servicos'));
    }

    public function detalheServico($idServico)
    {
        $servico = Servico::find($idServico);
        return view('site.servico.detalhe', compact('servico'));
    }
}
