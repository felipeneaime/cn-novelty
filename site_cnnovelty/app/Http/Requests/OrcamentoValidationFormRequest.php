<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrcamentoValidationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nomecliente'   => 'required|min:3|max:120',
            'logradouro'    => 'required',
            'cidade'        => 'required',
            'estado'        => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nomecliente.required' => 'O campo NOME é obrigatório',
            'logradouro.required' => 'O campo LOGRADOURO é obrigatório',
            'cidade.required' => 'O campo CIDADE é obrigatório',
            'estado.required' => 'O campo ESTADO é obrigatório',
        ];
    }
}
