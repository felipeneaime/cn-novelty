<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $fillable = [
        'nome_cliente',
        'logradouro',
        'cidade',
        'estado',
        'nomes_produtos',
        'quantidades_produtos',
        'totais_parciais',
        'total',
        'validade',
        'nome_funcionario'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
