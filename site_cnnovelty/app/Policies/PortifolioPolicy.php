<?php

namespace App\Policies;

use App\User;
use App\Portifolio;
use Illuminate\Auth\Access\HandlesAuthorization;

class PortifolioPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function editPortifolio(User $user, Portifolio $portifolio)
    {
        return $user->id == $portifolio->user_id;
    }
}
