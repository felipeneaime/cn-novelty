<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portifolio extends Model
{
    protected $fillable = [
        'titulo',
        'min_descricao',
        'empresa',
        'sobre_empresa',
        'imagem',
        'galeria_port',
        'descricao'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
