<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    protected $fillable = [
        'name',
        'valor',     
        'categoria_id',
        'quantidade_estoque'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
