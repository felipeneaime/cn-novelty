<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequisicaoSaidas extends Model
{
    protected $fillable = [
        'numero_nota_fiscal',
        'nomes_produtos',
        'quantidades_produtos',
        'lida',
        'atendida',
        'nome_funcionario'
    ];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
