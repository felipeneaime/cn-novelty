<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaidaEstoque extends Model
{
    protected $fillable = [
        'produto',
        'quantidade',     
        'motivo',
        'numero_requisicao',
        'funcionario'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
