<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $fillable = [
        'titulo',
        'min_descricao',
        'imagem',
        'descricao',
        'categoria'
    ];
}
