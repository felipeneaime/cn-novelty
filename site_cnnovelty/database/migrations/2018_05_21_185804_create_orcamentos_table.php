<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrcamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orcamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_cliente', 150);
            $table->string('logradouro');
            $table->string('cidade');
            $table->string('estado');
            $table->string('nomes_produtos');
            $table->string('quantidades_produtos');
            $table->string('totais_parciais');
            $table->float('total');
            $table->string('data_validade');
            $table->string('nome_funcionario', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orcamentos');
    }
}
