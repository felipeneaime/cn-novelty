<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisicaoSaidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisicao_saidas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_nota_fiscal');
            $table->string('nomes_produtos');
            $table->string('quantidades_produtos');
            $table->boolean('lida');
            $table->boolean('atendida');
            $table->string('nome_funcionario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisicao_saidas');
    }
}
