@extends('adminlte::page')

@section('title', 'CN Novelty - Administração')

@section('content_header')
    
@stop

@section('content')
<div class="col-md-3 col-sm-6 col-xs-12">
    <h3>Dashboard</h3>
    <div class="info-box">
    <span class="info-box-icon bg-red"><i class="fa fa-external-link-square"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Req. de saídas</span>
        <span class="info-box-number">{{$numero_saidas}}<small></small></span>
    </div>
    <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
</div>
@stop