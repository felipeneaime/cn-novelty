@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Categoria de Produtos')

@section('content_header')
    <h1>Categoria de Produtos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Categoria de Produtos</h3>
             
              @can('create', $categoriaprodutos)
              <a href="{{ route('create.categoria-produto')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus" style="margin-right:10px;"></i>
                Novo
              </a>
              @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th style="text-align:center">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($categoriaprodutos as $categoriaproduto)
                    <tr style="border-bottom: solid #ccc 2px">
                    @can('view', $categoriaproduto)
                        <th class="col-md-5" style="vertical-align: middle;">{{$categoriaproduto->id}}</th>
                        <th class="col-md-5" style="vertical-align: middle;">{{$categoriaproduto->name}}</th>
                        <th class="col-md-2" style="vertical-align: middle; text-align:center">
                          @can('edit', $categoriaproduto)
                          <a href="{{ route('edit.categoria-produto', $categoriaproduto->id) }}"type="button" class="btn btn-default btn-flat"><i class="fa fa-edit"></i></a>
                          @endcan
                          @can('delete', $categoriaproduto)
                          <a type="button" class="btn btn-danger btn-flat"><i class="fa fa-remove"></i></a>
                          @endcan
                    @endcan
                        </th>
                    </tr>
                @empty
                    <p>Nenhuma categoria cadastrada!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop