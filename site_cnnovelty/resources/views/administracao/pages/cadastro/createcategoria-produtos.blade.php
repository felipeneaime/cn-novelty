@extends('adminlte::page')

@section('title', 'CN Novelty - Criar Categoria de Produtos')

@section('content_header')
    <h1>Criar Categoria de Produtos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <form method="POST" action="{{ route('save.categoria-produto') }}">

                    <!-- Token oculto -->
                    {!! csrf_field() !!}

                    <!-- text input Titulo -->
                    <div class="form-group">
                        <label>Nome</label>
                        <input class="form-control" name="name" placeholder="" type="text">
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
                </form>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop