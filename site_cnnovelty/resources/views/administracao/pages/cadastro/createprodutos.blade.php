@extends('adminlte::page')

@section('title', 'CN Novelty - Criar Produtos')

@section('content_header')
    <h1>Criar Produtos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <form method="POST" action="{{ route('save.produto') }}">

                    <!-- Token oculto -->
                    {!! csrf_field() !!}

                    <!-- text input Name -->
                    <div class="form-group">
                        <label>Nome</label>
                        <input class="form-control" name="name" placeholder="" type="text">
                    </div>

                    <div class="form-group">
                        <label>Valor</label>
                        <input class="form-control" name="valor" placeholder="" type="text">
                    </div>

                    <div class="form-group">
                    <label>Categoria</label>
                    <select class="form-control" name="categoria">
                    @forelse($categoriaprodutos as $categoriaproduto)
                    <option value="{{$categoriaproduto->id}}">{{$categoriaproduto->name}}</option>
                    @empty
                    <p>Nenhuma categoria cadastrada!</p>
                    @endforelse
                    </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
                </form>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop