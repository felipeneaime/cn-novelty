@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Produtos')

@section('content_header')
    <h1>Produto "{{$produto->name}}"</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Produto "{{$produto->name}}"</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form method="POST" action="{{ route('update.produto', $produto->id) }}">

        <!-- Token oculto -->
        {!! csrf_field() !!}

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Nome</label>
          <input class="form-control" name="name" placeholder="" type="text" value="{{$produto->name}}">
        </div>
        
        <div class="form-group">
          <label>Valor</label>
          <input class="form-control" name="valor" placeholder="" type="text" value="{{$produto->valor}}">
        </div>

        <div class="form-group">
          <label>Categoria</label>
          <select class="form-control" name="categoria_id">
            @forelse($categoriaprodutos as $categoriaproduto)
            <option value="{{$categoriaproduto->id}}">{{$categoriaproduto->name}}</option>
            @empty
            <p>Nenhuma categoria cadastrada!</p>
            @endforelse
          </select>
        </div>
      </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
    </form>
</div>
@stop