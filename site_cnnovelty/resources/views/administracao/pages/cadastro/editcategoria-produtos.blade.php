@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Categoria de Produtos')

@section('content_header')
    <h1>Categoria "{{$categoriaproduto->name}}"</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Categoria "{{$categoriaproduto->name}}"</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form method="POST" action="{{ route('update.categoria-produto', $categoriaproduto->id) }}">

        <!-- Token oculto -->
        {!! csrf_field() !!}

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Titulo</label>
          <input class="form-control" name="name" placeholder="" type="text" value="{{$categoriaproduto->name}}">
        </div>
      </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
    </form>
</div>
@stop