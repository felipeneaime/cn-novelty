@extends('adminlte::page')

@section('title', 'CN Novelty - Criar de Portifolio')

@section('content_header')
    <h1>Criar Portifolio</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <form method="POST" action="{{ route('save.portifolio') }}">

                    <!-- Token oculto -->
                    {!! csrf_field() !!}

                    <!-- text input Titulo -->
                    <div class="form-group">
                        <label>Titulo</label>
                        <input class="form-control" name="titulo" placeholder="" type="text">
                    </div>

                    <!-- text input Descricao Minima -->
                    <div class="form-group">
                        <label>Descrição Minima</label>
                        <input class="form-control" name="min_descricao" placeholder="" type="text">
                    </div>

                    <!-- text input Empresa -->
                    <div class="form-group">
                        <label>Empresa</label>
                        <input class="form-control" name="empresa" placeholder="" type="text">
                    </div>

                    <!-- textarea Sobre a Empresa -->
                    <div class="form-group">
                        <label>Sobre a Empresa</label>
                        <textarea class="form-control"  name="sobre_empresa" rows="3" placeholder=""></textarea>
                    </div>

                    <!-- text input imagem -->
                    <div class="form-group">
                        <label>Imagem</label>
                        <input class="form-control" name="imagem" placeholder="" type="text">
                    </div>

                    <!-- textarea Descrição -->
                    <div class="form-group">
                        <label>Descrição</label>
                        <textarea class="form-control"  name="descricao" rows="3" placeholder=""></textarea>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
                </form>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop