@extends('adminlte::page')

@section('title', 'CN Novelty - Criar de Serviçoo')

@section('content_header')
    <h1>Criar Serviço</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <form method="POST" action="{{ route('save.servicos') }}">

                    <!-- Token oculto -->
                    {!! csrf_field() !!}

                    <!-- text input Titulo -->
                    <div class="form-group">
                        <label>Titulo</label>
                        <input class="form-control" name="titulo" placeholder="" type="text">
                    </div>

                    <!-- text input Descricao Minima -->
                    <div class="form-group">
                        <label>Descrição Minima</label>
                        <input class="form-control" name="min_descricao" placeholder="" type="text">
                    </div>

                    <!-- text input imagem -->
                    <div class="form-group">
                        <label>Imagem</label>
                        <input class="form-control" name="imagem" placeholder="" type="text">
                    </div>

                    <!-- textarea Descrição -->
                    <div class="form-group">
                        <label>Descrição</label>
                        <textarea class="form-control"  name="descricao" rows="3" placeholder=""></textarea>
                    </div>

                    <!-- text input Categoria -->
                    <div class="form-group">
                        <label>Categoria</label>
                        <input class="form-control" name="categoria" placeholder="" type="text">
                    </div>

                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Criar</button>
                </div>
                </form>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop