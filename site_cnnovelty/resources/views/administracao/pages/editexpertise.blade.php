@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Expersite')

@section('content_header')
    <h1>Expertise "{{$expertise->titulo}}"</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Espertise "{{$expertise->titulo}}"</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form method="POST" action="{{ route('update.expertise', $expertise->id) }}">

        <!-- Token oculto -->
        {!! csrf_field() !!}

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Titulo</label>
          <input class="form-control" name="titulo" placeholder="" type="text" value="{{$expertise->titulo}}">
        </div>

        <!-- textarea Descrição -->
        <div class="form-group">
          <label>Descrição</label>
          <textarea class="form-control"  name="descricao" rows="3" placeholder="">{{$expertise->descricao}}</textarea>
        </div>

        <!-- text input Icone -->
        <div class="form-group">
          <label>Icone</label>
          <input type="text" class="form-control"  name="icone" placeholder="" value="{{$expertise->icone}}">
        </div>
      </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
    </form>
</div>
@stop