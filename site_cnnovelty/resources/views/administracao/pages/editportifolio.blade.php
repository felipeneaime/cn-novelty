@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Portifolio')

@section('content_header')
    <h1>Portifolio "{{$portifolio->titulo}}"</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Portifolio "{{$portifolio->titulo}}"</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form method="POST" enctype="multipart/form-data" action="{{ route('update.portifolio', $portifolio->id) }}">

        <!-- Token oculto -->
        {!! csrf_field() !!}

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Titulo</label>
          <input class="form-control" name="titulo" placeholder="" type="text" value="{{$portifolio->titulo}}">
        </div>

        <!-- text input Descricao Minima -->
        <div class="form-group">
            <label>Descrição Minima</label>
            <input class="form-control" name="min_descricao" placeholder="" type="text" value="{{$portifolio->min_descricao}}">
        </div>

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Empresa</label>
          <input class="form-control" name="empresa" placeholder="" type="text" value="{{$portifolio->empresa}}">
        </div>

        <!-- textarea Sobre a Empresa -->
        <div class="form-group">
            <label>Sobre a Empresa</label>
            <textarea class="form-control"  name="sobre_empresa" rows="3" placeholder="">{{$portifolio->sobre_empresa}}</textarea>
        </div>

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Imagem de capa:</label>
          <input id="image" accept="image/*" type="file" name="image">
        </div>

        <div class="form-group">
          <label>Galeria de Imagens:</label>
          <input id="image" multiple="multiple" accept="image/*" type="file" name="galeria_port[]">
        </div>
        <!-- textarea Descrição -->
        <div class="form-group">
          <label>Descrição</label>
          <textarea class="form-control"  name="descricao" rows="3" placeholder="">{{$portifolio->descricao}}</textarea>
        </div>
      </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
    </form>
</div>
@stop