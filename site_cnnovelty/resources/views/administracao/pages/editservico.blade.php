@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Serviço')

@section('content_header')
    <h1>Serviço "{{$servico->titulo}}"</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Serviço "{{$servico->titulo}}"</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form method="POST" action="{{ route('update.servicos', $servico->id) }}">

        <!-- Token oculto -->
        {!! csrf_field() !!}

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Titulo</label>
          <input class="form-control" name="titulo" placeholder="" type="text" value="{{$servico->titulo}}">
        </div>

        <!-- text input Descricao Minima -->
        <div class="form-group">
            <label>Descrição Minima</label>
            <input class="form-control" name="min_descricao" placeholder="" type="text" value="{{$servico->min_descricao}}">
        </div>

        <!-- text input Titulo -->
        <div class="form-group">
          <label>Imagem</label>
          <input class="form-control" name="imagem" placeholder="" type="text" value="{{$servico->imagem}}">
        </div>

        <!-- textarea Descrição -->
        <div class="form-group">
          <label>Descrição</label>
          <textarea class="form-control"  name="descricao" rows="3" placeholder="">{{$servico->descricao}}</textarea>
        </div>

        <!-- textarea Categoria -->
        <div class="form-group">
        <label>Categoria</label>
        <input class="form-control" name="categoria" placeholder="" type="text" value="{{$servico->min_descricao}}">
    </div>
      </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
    </form>
</div>
@stop