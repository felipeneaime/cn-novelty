@extends('adminlte::page')

@section('title', 'CN Novelty - Status de Produtos')

@section('content_header')
    <h1>Status Produtos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Produtos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Quantidade Estoque</th>
                    <th>Categoria</th>
                    <th style="text-align:center">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($produtos as $produto)
                    <tr style="border-bottom: solid #ccc 2px">
                    @can('view', $produto)
                        <th class="col-md-1" style="vertical-align: middle;">{{$produto->id}}</th>
                        <th class="col-md-4" style="vertical-align: middle;">{{$produto->name}}</th>
                        <th class="col-md-2" style="vertical-align: middle;">{{$produto->quantidade_estoque}}</th>

                        <th class="col-md-3" style="vertical-align: middle;">
                        @forelse($categoriaprodutos as $categoriaproduto)
                          @if($categoriaproduto->id == $produto->categoria_id)
                          {{$categoriaproduto->name}}
                          @endif
                        @empty
                            <p>Nenhum produto cadastrado!</p>
                        @endforelse
                        
                        </th>
 
                        <th class="col-md-4" style="vertical-align: middle; text-align:center">
                          @can('edit', $produto)
                          <a href="{{ route('visualizarHistorico.produto', $produto->id) }}"type="button" class="btn btn-default btn-flat"><i class="fa fa-search"></i> Visualizar Historico</a>
                          @endcan
                    @endcan
                        </th>
                    </tr>
                @empty
                    <p>Nenhum produto cadastrado!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop