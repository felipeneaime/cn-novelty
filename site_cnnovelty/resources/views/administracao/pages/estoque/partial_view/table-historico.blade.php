<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

<div class="box">

<div>
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Entradas</a></li>
        <li><a href="#tab_2" data-toggle="tab">Saídas</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
        <b>Entradas de Produtos:</b>

        <div class="box-body table-responsive">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <div class="row">
                <div class="col-xs-6">
                    <div id="example1_length" class="dataTables_length"></div>
                </div>
            </div>
            <table id="example1" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 187px;">
                            Data da Entrada
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 273px;">
                            Numero da Nota
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 252px;">
                            Motivo
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 160px;">
                            Funcionário
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 116px;">
                            Quantidade
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th rowspan="1" colspan="1">
                            Data da Entrada
                        </th>
                        <th rowspan="1" colspan="1">
                            Numero da Nota
                        </th>
                        <th rowspan="1" colspan="1">
                            Motivo
                        </th>
                        <th rowspan="1" colspan="1">
                            Funcionário
                        </th>
                        <th rowspan="1" colspan="1">
                            Quantidade
                        </th>
                    </tr>
                </tfoot>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                @forelse($movimentacoesEntradas as $movimentacaoEntradas)
                    <tr class="odd">
                        <td class="  sorting_1">{{$movimentacaoEntradas->created_at}}</td>
                        <td class=" ">{{$movimentacaoEntradas->numero_nota}}</td>
                        <td class=" ">{{$movimentacaoEntradas->motivo}}</td>
                        <td class=" ">{{$movimentacaoEntradas->funcionario}}</td>
                        <td class=" ">{{$movimentacaoEntradas->quantidade}}</td>
                    </tr>
                    @empty

                    @endforelse

                </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6"></div>
                <div class="col-xs-6">
                    <div class="dataTables_paginate paging_bootstrap">
                </div>
            </div>
        </div>
        </div>
        </div>
        <!-- /.box-body -->
        </div>

        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
        <b>Saída de Produtos:</b>
        <div class="box-body table-responsive">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline" role="grid">
            <div class="row">
                <div class="col-xs-6">
                    <div id="example1_length" class="dataTables_length"></div>
                </div>
            </div>
            <table id="example2" class="table table-bordered table-striped dataTable" aria-describedby="example1_info">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 187px;">
                            Data da Entrada
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 273px;">
                            Numero da Nota
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 252px;">
                            Motivo
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 160px;">
                            Funcionário
                        </th>
                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 116px;">
                            Quantidade
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th rowspan="1" colspan="1">
                            Data da Entrada
                        </th>
                        <th rowspan="1" colspan="1">
                            Numero da Nota
                        </th>
                        <th rowspan="1" colspan="1">
                            Motivo
                        </th>
                        <th rowspan="1" colspan="1">
                            Funcionário
                        </th>
                        <th rowspan="1" colspan="1">
                            Quantidade
                        </th>
                    </tr>
                </tfoot>
                <tbody role="alert" aria-live="polite" aria-relevant="all">
                @forelse($movimentacoesSaidas as $movimentacaoSaidas)
                    <tr class="odd">
                        <td class="  sorting_1">{{$movimentacaoSaidas->created_at}}</td>
                        <td class=" ">{{$movimentacaoSaidas->numero_requisicao}}</td>
                        <td class=" ">{{$movimentacaoSaidas->motivo}}</td>
                        <td class=" ">{{$movimentacaoSaidas->funcionario}}</td>
                        <td class=" ">{{$movimentacaoSaidas->quantidade}}</td>
                    </tr>
                    @empty
                    @endforelse

                </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6"></div>
                <div class="col-xs-6">
                    <div class="dataTables_paginate paging_bootstrap">
                </div>
            </div>
        </div>
        </div>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</div>

<script>
$(document).ready(function() {
    $('#example1').DataTable();
} );

$(document).ready(function() {
    $('#example2').DataTable();
} );
</script>