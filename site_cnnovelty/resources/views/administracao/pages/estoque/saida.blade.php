
@extends('adminlte::page')

@section('title', 'CN Novelty - Saida de Estoque')

@section('content_header')
    <h1>Saida de Estoque</h1>
@stop

@section('content')
<form method="POST" action="{{ route('saveSaida.estoque') }}">
{!! csrf_field() !!}
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
              <tr>
                <td class="col-md-4" style="vertical-align: middle;">
                <label>Produto</label>
                <select id="teste" class="form-control select2" name="produto">
                    <option value="">Selecione o produto</option>
                    @forelse($produtos as $produto)
                        <option value="{{$produto->id}}">{{$produto->name}}</option>
                    @empty <p>Nenhum produto cadastrado!</p>
                    @endforelse
                </select>
                </td>
                
                <td class="col-md-2" style="vertical-align: middle;">
                <label>Quantidade</label>
                <input class="form-control" name="quantidade" placeholder="" type="text">
                </td>
                
                <td class="col-md-3" style="vertical-align: middle;">
                <label>Motivo</label>
                <input class="form-control" name="motivo" placeholder="" type="text">
                </td>

                <td class="col-md-4" style="vertical-align: middle;">
                <label>Numero da Requisição</label>
                <input class="form-control" name="numero_requisicao" placeholder="" type="text">
                </td>

              </tr>
              </table>
              <br>
              <button type="submit" class="btn btn-primary">Dar saída em estoque</button>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
          </div>
        <!-- /.col -->
        </div>
      </div>
      <!-- /.row -->
      </form>
@stop