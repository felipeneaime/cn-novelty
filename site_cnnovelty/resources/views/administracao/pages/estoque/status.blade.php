@extends('adminlte::page')

@section('title', 'CN Novelty - Estatus Produto')

@section('content_header')
    <h1>Status Produto {{$produto->name}}</h1>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
        <div class="box-header">
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Categoria</th>
                    <th>Quantidade Estoque</th>
                </tr>
            </thead>
            <tbody>
                <tr style="border-bottom: solid #ccc 2px">
                @can('view', $produto)
                    <th class="col-md-1" style="vertical-align: middle;">{{$produto->id}}</th>
                    <th class="col-md-4" style="vertical-align: middle;">{{$produto->name}}</th>
                    <th class="col-md-3" style="vertical-align: middle;">
                        @forelse($categoriaprodutos as $categoriaproduto)
                        @if($categoriaproduto->id == $produto->categoria_id)
                        {{$categoriaproduto->name}}
                        @endif
                        @empty
                            <p>Nenhum produto cadastrado!</p>
                        @endforelse
                    </th>
                    <th class="col-md-2" style="vertical-align: middle;">{{$produto->quantidade_estoque}}</th>
                @endcan
                </tr>
            </tbody>
        </table>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
        </div>
        @include('administracao.pages.estoque.partial_view.table-historico')
    <!-- /.col -->
    </div>
</div>
<!-- /.row -->
@stop