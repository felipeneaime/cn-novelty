@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Expertise')

@section('content_header')
    <h1>Expertise</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Expertise</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>Titulo</th>
                    <th>Descrição</th>
                    <th>Icone</th>
                    <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($expertises as $expertise)
                    <tr style="border-bottom: solid #ccc 2px">
                        <th class="col-md-2">{{$expertise->titulo}}</th>
                        <th class="col-md-6">{{$expertise->descricao}}</th>
                        <th class="col-md-2">{{$expertise->icone}}</th>
                        <th class="col-md-2">
                            <a href="{{url("/administracao/expertise/$expertise->id/edit")}}" class="btn btn-primary glyphicon glyphicon-edit"></a>
                        </th>
                    </tr>
                @empty
                    <p>Nenhuma expertise cadastrada!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop