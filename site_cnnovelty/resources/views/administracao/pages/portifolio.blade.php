@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Portifolio')

@section('content_header')
    <h1>Portifolio</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Portifolio</h3>
              @forelse($portifolios as $portifolio)
              @empty
                    <p>Você não tem permissão apra cadastrar</p>
              @endforelse
              @can('create', $portifolio)
              <a href="{{ route('create.portifolio')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus" style="margin-right:10px;"></i>
                Novo
              </a>
              @endcan
              
              
              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>Titulo</th>
                    <th>Empresa</th>
                    <th>Autor</th>
                    <th style="text-align:center">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($portifolios as $portifolio)
                    <tr style="border-bottom: solid #ccc 2px">
                    @can('view', $portifolio)
                        <th class="col-md-5" style="vertical-align: middle;">{{$portifolio->titulo}}</th>
                        <th class="col-md-5" style="vertical-align: middle;">{{$portifolio->empresa}}</th>
                        <th class="col-md-5" style="vertical-align: middle;">{{$portifolio->user->name}}</th>
                        <th class="col-md-2" style="vertical-align: middle; text-align:center">
                          
                          <a href="{{ route('edit.portifolio', $portifolio->id) }}"type="button" class="btn btn-default btn-flat"><i class="fa fa-edit"></i></a>
                          @can('delete', $portifolio)
                          <a type="button" class="btn btn-danger btn-flat"><i class="fa fa-remove"></i></a>
                          @endcan
                    @endcan
                        </th>
                    </tr>
                @empty
                    <p>Nenhuma expertise cadastrada!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop