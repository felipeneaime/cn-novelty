@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Portifolio')

@section('content_header')
    <h1>Portifolio</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Serviços</h3>
              <a href="{{ route('create.servicos')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus" style="margin-right:10px;"></i>
                Novo
              </a>  
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>Titulo</th>
                    <th>Categoria</th>
                    <th style="text-align:center">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($servicos as $servico)
                    <tr style="border-bottom: solid #ccc 2px">
                        <th class="col-md-5" style="vertical-align: middle;">{{$servico->titulo}}</th>
                        <th class="col-md-5" style="vertical-align: middle;">{{$servico->categorias}}</th>
                        <th class="col-md-2" style="vertical-align: middle; text-align:center">
                          <a href="{{ route('edit.servicos', $servico->id) }}"type="button" class="btn btn-default btn-flat"><i class="fa fa-edit"></i></a>
                          <a type="button" class="btn btn-danger btn-flat"><i class="fa fa-remove"></i></a>
                        </th>
                    </tr>
                @empty
                    <p>Nenhuma expertise cadastrada!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop