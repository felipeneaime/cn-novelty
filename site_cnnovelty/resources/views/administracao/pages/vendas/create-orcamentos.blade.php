
@extends('adminlte::page')

@section('title', 'CN Novelty - Criar Orçamentos')

@section('content_header')
    <h1>Criar Orçamentos</h1>
@stop

@section('content')
<form method="POST" action="{{ route('save.orcamento') }}">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            @include('administracao.pages.includes.alerts')

              <table class="table table-bordered table-hover">
              <tr>
                <td class="col-md-5" style="vertical-align: middle;">
                <label>Nome Cliente</label>
                <input class="form-control" name="nomecliente" placeholder="" type="text">
                </td>
                <td class="col-md-5" style="vertical-align: middle;">
                <label>Logradouro</label>
                <input class="form-control" name="logradouro" placeholder="" type="text">
                </td>
                <td class="col-md-5" style="vertical-align: middle;">
                <label>Cidade</label>
                <input class="form-control" name="cidade" placeholder="" type="text">
                </td>
                <td class="col-md-5" style="vertical-align: middle;">
                <label>Estado</label>
                <input class="form-control" name="estado" placeholder="" type="text">
                </td>
              </tr>
              </table>
              <!-- /.box-body -->
              </div>
          <!-- /.box -->
          </div>
        <!-- /.col -->
        </div>
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
                    <tr>
                    <a href="#" class="btn btn-success addprod">Adicionar Produto</button></a>
                    </tr>
                    <tr>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Valor</th>
                    <th>Remover</th>
                    </tr>              
                    <!-- Token oculto -->
                    <tr>
                    {!! csrf_field() !!}
                    </tr>
                    <tbody id="produtos">
                    <tr style="display:none">
                      <th>
                        <select id="teste" onchange="troca(this.value,0 )" class="form-control select2" name="produto[0]">
                          <option>Selecione o produto</option>
                        </select>
                      </th>
                      <th>
                        <input class="form-control" name="qtd[0]" placeholder="" type="text">
                      </th>
                      <th>
                      <input class="form-control" id="valor[0]" name="valor[0]" placeholder="" type="text" readonly="true">
                      </th>
                    </tr>
                    </tbody>
              </table>
              <button type="submit" class="btn btn-primary">Criar</button>
              <br><br>
            </div>
          </div>
        </div>
      </div>
      </form>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script type="text/javascript">
RemoveTableRow = function(handler) {
    var tr = $(handler).closest('tr');
    tr.fadeOut(400, function() {
        tr.remove();
    });
    return false;
};
    $(document).ready(function(){


        var i=1;
        $(".addprod").click(function(){
          $(document).ready(function() {
              $('.select2').select2();
          });

            var newRow = $("<tr></tr>");
            var cols = "";
            var produto = i;
            
            cols += '<tr><th><select style="width:100%" id="teste" onchange="troca(this.value,'+produto+' )" class="form-control select2" name="produto['+i+']"> <option value="">Selecione o produto</option> @forelse($produtos as $produto) <option value="{{$produto->id}}:{{$produto->name}}:{{$produto->valor}}">{{$produto->name}}</option> @empty <p>Nenhum produto cadastrado!</p> @endforelse </select></th>';
            cols += '<th><input class="form-control" name="qtd['+i+']" placeholder="" type="text"></th>';
            cols += '<th><input class="form-control" id="valor['+i+']" name="valor['+i+']" placeholder="" type="text" readonly="true"></th>';
            cols += '<th>';
            cols += '<button onclick="RemoveTableRow(this)" class="btn btn-danger waves-effect w-md waves-light m-b-5" type="button">Remover</button>';
            cols += '</th></tr>';
            i++;
            newRow.append(cols);
        
            $("#produtos").append(cols);
        });
    });
    function troca(val, id) {
      // alert(val);
      var teste = val.split(":");
      var valor = teste[2];
      //alert(valor);
      // var desc = val.split(',');
      // var valor = des[1];
      //valor = valor[2];
      document.getElementById("valor["+id+"]").value = valor;
      //document.getElementById("valor[1]").value = x;
    }
</script>

@stop
