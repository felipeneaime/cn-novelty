@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Orçamentos')

@section('content_header')
    <h1>Orçamentos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Tabela de Orçamentos</h3>
              @forelse($orcamentos as $orcamento)
              @empty
              @endforelse
              @can('create', $orcamentos)
              <a href="{{ route('create.orcamento')}}" class="btn btn-success pull-right">
                <i class="fa fa-plus" style="margin-right:10px;"></i>
                Novo
              </a>
              @endcan
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            @include('administracao.pages.includes.alerts')
              <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                    <th>Código Orçamento</th>
                    <th>Nome do Cliente</th>
                    <th>Nome do Funcionario</th>
                    <th style="text-align:center">Ação</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($orcamentos as $orcamento)
                    <tr style="border-bottom: solid #ccc 2px">
                    @can('view', $orcamento)
                        <th class="col-md-2" style="vertical-align: middle;">{{$orcamento->id}}</th>
                        <th class="col-md-4" style="vertical-align: middle;">{{$orcamento->nome_cliente}}</th>
                        <th class="col-md-4" style="vertical-align: middle;">{{$orcamento->nome_funcionario}}</th>
                        <th class="col-md-5" style="vertical-align: middle; text-align:center">
                          <a href="{{ route('view.orcamento', $orcamento->id) }}"type="button" class="btn btn-default btn-flat"><i class="fa fa-search"></i></a>
                          @can('delete', $orcamento)
                          <a type="button" class="btn btn-danger btn-flat"><i class="fa fa-remove"></i></a>
                          @endcan
                    @endcan
                        </th>
                    </tr>
                @empty
                    <p>Nenhum orçamento cadastrado!</p>
                @endforelse
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop