<?php
   $nomes_produtos;
   $list_produtos = explode(":", $nomes_produtos);
   $quantidades_produtos = explode(":", $quantidades_produtos); ;
   $valores_produtos = explode(":", $valores_produtos);
   $totais_parciais = explode(":", $totais_parciais);

   //dd($list_produtos);
   // dd(count($list_produtos));
    $numero_produtos = count($list_produtos);
    // dd($numero_produtos);
?>

<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<title> Orçamento {{$id}}</title>
  <style ='text/css'>
    body {
      font-family: Calibri, DejaVu Sans, Arial;
      margin: 0;
      padding: 0;
      border: none;
      font-size: 13px;
    }

    #exemplo {
      width: 100%;
      height: auto;
      overflow: hidden;
      padding: 5px 0;
      text-align: center;
      background-color: ;
      color: #FFF;
    }
    #cabecario {
      width: 100%;
      height: auto;
      overflow: hidden;
      padding: 5px 5px;
      text-align: left;
      /* background-color: #ccc; */
      color: #000;
    }
    .table-cabecario {
        width:100%;
        /* border: 1px solid; */
    }
    .produtos {
        margin-top: 10px;
    }
    .cabecario-produtos {
        background: #f89a46;
        height: auto;
        color: #fff;
        vertical-align: middle;
    }

    .normas {
        width:100%;
        padding: 5px 5px;
        /* border: 1px solid; */
    }
    .normas p {
        text-align: justify;
    }

    .rodape {
        width:100%;
        /* border: 1px solid; */
    }

    .tr-cliente {
        text-align: left;
    }

    .tr-funcionario {
        text-align: right;
    }


  </style>
</head>
<body>
  <div id='exemplo'>
    <img src="http://cnnovelty.com.br/assets/img/logo.png" alt="">
  </div>
  <div id='cabecario'>
    <table class="table-cabecario">
        <tr><td colspan="2" style="text-align:center"><b>Código do orçamento:</b> {{$id}}<td></tr>
        <tr>
            <td><b>Nome:</b> {{$nome_cliente}}</td>
            <td style="text-align:right"><b>Endereço:</b> {{$logradouro}}</td>
        </tr>
        <tr>
        <td><b>Cidade:</b> {{$cidade}}</td>
        <td style="text-align:right"><b>Estado:</b> {{$estado}}</td>
        </tr>
    </table>
  </div>
  <div class="produtos">
    <table class="table">
        <thead class="cabecario-produtos">
        <tr>
            <th scope="col">Produtos</th>
            <th scope="col">Quantidade</th>
            <th scope="col">Valor</th>
            <th scope="col">Total Parcial</th>
        </tr>
        </thead>
        <tbody>
        @for ($i = 1; $i < $numero_produtos; $i++)
        <tr>
            <th>{{ $list_produtos[$i] }}</th>
            <td>{{ $quantidades_produtos[$i] }}</td>
            <td>R$ {{ $valores_produtos[$i] }}</td>
            <td>R$ {{ $totais_parciais[$i] }}</td>
        </tr>
        @endfor
        <thead class="cabecario-produtos">
        <tr>
            <th scope="col">Total:</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col">R$ {{$total}}</th>
        </tr>
        </thead>
        </tbody>
    </table>
  </div>

  <div id='normas'>
    
    <p>
        <b>Objetivo:</b><br>
        Essa proposta tem por objetivo disponibilizar e/ou fornecer os produtos citados acima pela empresa CN Novelty. O período/tempo para produção dos itens da proposta tem o prazo de 15 dias úteis a contar a partir do aceite do Cliente.
    </p>
    
    <p>
        <b>Validade da Proposta:</b><br>
        Essa proposta tem a validade de 10 dias corridos a contar da data da emissão da proposta. 
    </p>
    
    <p>
        <b>Condições de Pagamento:</b><br>
        A forma de pagamento pode ser afetuada através de deposito bancario (Valor avista) ou em 3 vezes do cartão sem juros.
    </p>

    <p>
        <b>Aceite da proposta:</b><br>
        O Cliente declara que reviu os termos e condições e apõe sua assinatura abaixo, a fim de consignar sua concordância com os termos e condições desta proposta.
    </p>
  </div>
  <table class="rodape">
        <tr>
            <td class="tr-cliente">____________________</td>
            <td class="tr-funcionario">____________________</td>
        </tr>
        <tr>
            <td class="tr-cliente">{{$nome_cliente}}</td>
            <td class="tr-funcionario">{{$nome_funcionario}}</td>
        </tr>
    </table>
</body>
</html>