<?php
   $nomes_produtos;
   $list_produtos = explode(":", $nomes_produtos);
   $quantidades_produtos = explode(":", $quantidades_produtos); ;


   //dd($list_produtos);
   // dd(count($list_produtos));
    $numero_produtos = count($list_produtos);
    // dd($numero_produtos);
?>

<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<title> Requisicao {{$id}}</title>
  <style ='text/css'>
    body {
      font-family: Calibri, DejaVu Sans, Arial;
      margin: 0;
      padding: 0;
      border: none;
      font-size: 13px;
    }

    #exemplo {
      width: 100%;
      height: auto;
      overflow: hidden;
      padding: 5px 0;
      text-align: center;
      background-color: ;
      color: #FFF;
    }
    #cabecario {
      width: 100%;
      height: auto;
      overflow: hidden;
      padding: 5px 5px;
      text-align: left;
      /* background-color: #ccc; */
      color: #000;
    }
    .table-cabecario {
        width:100%;
        /* border: 1px solid; */
    }
    .produtos {
        margin-top: 10px;
    }
    .cabecario-produtos {
        background: #f89a46;
        height: auto;
        color: #fff;
        vertical-align: middle;
    }

    .normas {
        width:100%;
        padding: 5px 5px;
        /* border: 1px solid; */
    }
    .normas p {
        text-align: justify;
    }

    .rodape {
        width:100%;
        /* border: 1px solid; */
    }

    .tr-cliente {
        text-align: left;
    }

    .tr-funcionario {
        text-align: right;
    }


  </style>
</head>
<body>
  <div id='exemplo'>
    <img src="http://cnnovelty.com.br/assets/img/logo.png" alt="">
  </div>
  <div id='cabecario'>
    <table class="table-cabecario">
        <tr><td colspan="2" style="text-align:center"><b>Código da Requisicao:</b> {{$id}}<td></tr>
        <tr>
            <td><b>Nome:</b> {{$nome_funcionario}}</td>
        </tr>
        <tr>
        </tr>
    </table>
  </div>
  <div class="produtos">
    <table class="table">
        <thead class="cabecario-produtos">
        <tr>
            <th scope="col">Produtos</th>
            <th scope="col">Quantidade</th>
        </tr>
        </thead>
        <tbody>
        @for ($i = 1; $i < $numero_produtos; $i++)
        <tr>
            <th>{{ $list_produtos[$i] }}</th>
            <td>{{ $quantidades_produtos[$i] }}</td>
        </tr>
        @endfor
        <thead class="cabecario-produtos">
        </thead>
        </tbody>
    </table>
  </div>

  <div id='normas'>
    
    <p>
        <b>Objetivo:</b><br>
        Este documento tem por objetivo, registrar a retirada de produtos pelo vendedor solicitante
    </p>

    <p>
        <b></b><br>
        Declaro que sou responsavel por esses produtos a partir do momento da assinatura. Qualquer sumiço ou quebra de produtos dentro da loja é de minha responsabilidade.
    </p>

  </div>
  <table class="rodape">
        <tr>
            <td class="tr-cliente">____________________</td>
        </tr>
        <tr>
            <td class="tr-cliente">{{$nome_funcionario}}</td>
        </tr>
    </table>
</body>
</html>