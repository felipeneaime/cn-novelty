<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
@extends('adminlte::page')

@section('title', 'CN Novelty - Lista de Orçamentos')

@section('content_header')
    <h1>Requisições de Saidas</h1>
@stop

@section('content')
<div class="box">
<div class="box-header">
  <h3 class="box-title">Solicitações de saída</h3>
  <a href="{{ route('create.requisicao-saidas')}}" class="btn btn-success pull-right">
    <i class="fa fa-plus" style="margin-right:10px;"></i>
    Nova
  </a>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    
    </div>
    <div class="row">
        <div class="col-sm-12">
        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
        <thead>
            <tr role="row">
                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 182px;">
                    Data da Requisição
                </th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 224px;">
                    Numero Nota Fiscal
                </th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 199px;">
                    Lida
                </th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 156px;">
                    Funcionario
                </th>
                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">
                    Ação
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach($saidas as $saida)
        <tr role="row" class="odd">
        <td class="sorting_1">{{$saida->created_at}}</td>
        <td>{{$saida->numero_nota_fiscal}}</td>
        <td>
            @if($saida->lida == 1)
                Sim
            @endif
            @if($saida->lida == 0)
                Não
            @endif
        </td>
        <td>{{$saida->nome_funcionario}}</td>
        <td></td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th rowspan="1" colspan="1">Data da Requisição</th>
            <th rowspan="1" colspan="1">Numero Nota Fiscal</th>
            <th rowspan="1" colspan="1">Lida</th>
            <th rowspan="1" colspan="1">Funcionario</th>
            <th rowspan="1" colspan="1">Ação</th>
        </tr>
        </tfoot>
        </table>
    </div>
</div>

</div>
</div>
<!-- /.box-body -->
</div>
@stop
<style>
.old{
    background:#000;
}
</style>
<script>
$(document).ready(function() {
    $('#example1').DataTable();
} );
</script>