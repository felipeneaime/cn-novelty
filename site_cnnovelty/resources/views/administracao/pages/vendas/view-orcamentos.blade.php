@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Portifolio')

@section('content_header')
    <h1>Orçamento</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Orçamento {{$orcamentos->id}}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-hover">
        <tr>
        <td class="col-md-5" style="vertical-align: middle;">
        <label>Nome Cliente</label>
        <input class="form-control" name="nomecliente" placeholder="" disabled type="text" value="{{ $orcamentos->nome_cliente }}">
        </td>
        <td class="col-md-5" style="vertical-align: middle;">
        <label>Logradouro</label>
        <input class="form-control" name="logradouro" placeholder="" disabled type="text" value="{{$orcamentos->logradouro}}">
        </td>
        <td class="col-md-5" style="vertical-align: middle;">
        <label>Cidade</label>
        <input class="form-control" name="cidade" placeholder="" disabled type="text" value="{{$orcamentos->cidade}}">
        </td>
        <td class="col-md-5" style="vertical-align: middle;">
        <label>Estado</label>
        <input class="form-control" name="estado" placeholder="" disabled type="text" value="{{$orcamentos->estado}}">
        </td>
        </tr>
        </table>
    <!-- /.box-body -->
    </div>
    <div class="box-body">
        <table class="table table-bordered table-hover">
            <tr>
            <th>Produto</th>
            <th>Quantidade</th>
            <th>Valor</th>
            <th>Remover</th>
            </tr>              
            <!-- Token oculto -->
            <th>
            @foreach($produtos as $produto)
            @if($produto != null)
            <input class="form-control" name="produto" disabled value="" placeholder="{{$produto}}" type="text">
            @endif
            @endforeach
            </th>
            <th>
            @foreach($quantidades as $quantidade)
            @if($quantidade != null)
            <input class="form-control" name="quantidade" disabled placeholder="{{$quantidade}}" type="text">
            @endif
            @endforeach
            </th>
            <th>
            @foreach($valores as $valor)
            @if($valor != null)
            <input class="form-control" name="valores" disabled placeholder="R$ {{$valor}}" type="text">
            @endif
            @endforeach
            </th>
            <th>
            @foreach($totais_parciais as $total_parcial)
            @if($total_parcial!= null)
            <input class="form-control" name="totais_parciais" disabled placeholder="R$ {{$total_parcial}}" type="text">
            @endif
            @endforeach
            </th>
            <tr>
            <th></th>
            <th></th>
            <th style="text-align:right;">TOTAL:</th>
            <th>R$ {{$orcamentos->total}}</th>
            </tr>   
        </table>
        <br>
         <a href="{{ route('pdf.orcamento', $orcamentos->id) }}" type="button" class="btn btn-danger btn-flat"><i class="fa  fa-file-pdf-o"></i></a>
    </div>
</div>
@stop