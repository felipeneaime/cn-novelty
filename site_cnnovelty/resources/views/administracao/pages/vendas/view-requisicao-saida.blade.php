@extends('adminlte::page')

@section('title', 'CN Novelty - Adminstração - Edição de Portifolio')

@section('content_header')
    <h1>Orçamento</h1>
@stop

@section('content')
<div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Requisicao {{$requisicao->id}}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-hover">
        <tr>
        <td class="col-md-5" style="vertical-align: middle;">
        <label>Nome Funcionario</label>
        <input class="form-control" name="nomecliente" placeholder="" disabled type="text" value="{{ $requisicao->nome_funcionario }}">
        </td>
        </tr>
        </table>
    <!-- /.box-body -->
    </div>
    <div class="box-body">
        <table class="table table-bordered table-hover">
            <tr>
            <th>Produto</th>
            <th>Quantidade</th>
            </tr>              
            <!-- Token oculto -->
            <th>
            @foreach($produtos as $produto)
            @if($produto != null)
            <input class="form-control" name="produto" disabled value="" placeholder="{{$produto}}" type="text">
            @endif
            @endforeach
            </th>
            <th>
            @foreach($quantidades as $quantidade)
            @if($quantidade != null)
            <input class="form-control" name="quantidade" disabled placeholder="{{$quantidade}}" type="text">
            @endif
            @endforeach
            </th>
        </table>
        <br>
         <a href="{{ route('pdf.requisicao', $requisicao->id) }}" type="button" class="btn btn-danger btn-flat"><i class="fa  fa-file-pdf-o"></i></a>
    </div>
</div>
@stop