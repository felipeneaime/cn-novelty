@extends('adminlte::page')

@section('title', 'CN Novelty - Editar Perfil de Usuário')

@section('content_header')
    <h1>Editar Perfil</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover">
              @include('administracao.pages.includes.alerts')
                <form method="POST" enctype="multipart/form-data" action="{{ route('profile.update') }}">
                  <!-- Token oculto -->
                  {!! csrf_field() !!}

                  <!-- text input Name -->
                  <div class="form-group">
                    <label>Nome</label>
                    <input class="form-control" value="{{ auth()->user()->name }}" name="name" placeholder="Nome" type="text">
                  </div>

                  <div class="form-group">
                    <label>E-mail</label>
                    <input disabled class="form-control" value="{{ auth()->user()->email }}" name="nome" placeholder="" type="text">
                  </div>

                  <div class="form-group">
                    <label>Senha</label>
                    <input class="form-control" name="password" placeholder="" type="password">
                  </div>

                  <div class="form-group">
                    <label>Imagem</label>
                    <input class="" name="image" placeholder="" type="file">

                    @if(auth()->user()->image != null)
                    <img src="{{ url('site_cnnovelty/public/storage/users/'.auth()->user()->image) }}" class="img-circle" style="width:80px; height:80px;margin-top:5px;" alt="User Image">
                    @endif
                  </div>

                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                  </div>

                </form>
              </table>
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
@stop