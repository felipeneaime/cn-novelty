<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
@include('layout.app')
</head>

<body class="home">
	@include('partial_view.menu-superior')
	<!-- Header -->
	
	<!-- Intro -->
	<div class="container text-center">
	<br /><br /><br />
	</div>
	<!-- /Intro-->
	<!-- container -->
	<div class="container">
			<div class="row">
				<!-- Article main content -->
				<article class="col-sm-9 maincontent">
					<header class="page-header">
						<h1 class="page-title">Contato</h1>
					</header>
					
					<p>
					Tem sugestão ou crítica para nós? Preencha o formulário e envie-nos sua mensagem.
					</p>
					<br>
						<form>
							<div class="row">
								<div class="col-sm-4">
									<input class="form-control" type="text" placeholder="Name">
								</div>
								<div class="col-sm-4">
									<input class="form-control" type="text" placeholder="Email">
								</div>
								<div class="col-sm-4">
									<input class="form-control" type="text" placeholder="Phone">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-12">
									<textarea placeholder="Type your message here..." class="form-control" rows="9"></textarea>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-sm-6">
									<label class="checkbox"><input type="checkbox"> Sign up for newsletter</label>
								</div>
								<div class="col-sm-6 text-right">
									<input class="btn btn-action" type="submit" value="Send message">
								</div>
							</div>
						</form>
	
				</article>
				<!-- /Article -->
				
				<!-- Sidebar -->
				<aside class="col-sm-3 sidebar sidebar-right">
	
					<div class="widget">
						<h4>Address</h4>
						<address>
							2002 Holcombe Boulevard, Houston, TX 77030, USA
						</address>
						<h4>Phone:</h4>
						<address>
							(713) 791-1414
						</address>
					</div>
	
				</aside>
				<!-- /Sidebar -->
	
			</div>
		</div>	<!-- /container -->

	<!-- Social links. @TODO: replace by link/instructions in template -->
	<section id="social">
		<div class="container">
			<div class="wrapper clearfix">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
				</div>
				<!-- AddThis Button END -->
			</div>
		</div>
	</section>
	<!-- /social links -->

	@include('partial_view.footer')

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>