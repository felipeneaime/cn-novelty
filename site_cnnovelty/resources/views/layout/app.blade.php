
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="Felipe Neaime">

<title>CN Novelty - Aperte o Play para o sucesso</title>

<link rel="shortcut icon" href="{{asset('assets/img/cn-faveicon.png')}}">

<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<!-- Custom styles for our template -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-theme.css')}}" media="screen" >
<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->