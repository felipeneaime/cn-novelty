	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-8 widget">
						<h3 class="widget-title">Contato</h3>
						<div class="widget-body">
							<p>(19) 98168-2264<br>
								<a href="mailto:#">contato@cnnovelty.com.br</a><br>
							</p>	
						</div>
					</div>

					<!--
					<div class="col-md-3 widget">
						<h3 class="widget-title">Siga-nos</h3>
						<div class="widget-body">
							<p class="follow-me-icons">
								<i class="fab fa-twitter fa-2"></i>
                                <i class="fab fa-facebook"></i>
								<i class="fab fa-instagram"></i>
							</p>	
						</div>
					</div>
					-->

				</div>
				<!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<!-- <a href="contact.html">Contato</a> -->
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; <?php echo date('Y'); ?>
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

	</footer>	