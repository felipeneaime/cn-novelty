<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
@include('layout.app')
</head>

<body class="home">
	@include('partial_view.menu-superior')
	<!-- Header -->
	<header id="head">
		<div class="container">
			<div class="row">
				<h1 class="lead">Aperte o play para o sucesso</h1>
				<!--
				<p class="tagline">Siga a CN Novelty no Facebook<a href="http://facebook.com.br"> Aqui!</a></p>
				<p><a class="btn btn-default btn-lg" role="button">MORE INFO</a> <a class="btn btn-action btn-lg" role="button">DOWNLOAD NOW</a></p>
				-->
			</div>
		</div>
	</header>
	<!-- /Header -->

	<!-- Intro -->
	<div class="container text-center">
		<br> <br>
		<h2 class="thin">Soluções empresarias e residencias</h2>
		<p class="text-muted">
			Confira nossas soluções para sua casa ou empresa, com o melhor custo/beneficio
		</p>
	</div>
	<!-- /Intro-->
		
	<!-- Highlights - jumbotron -->
	<div class="jumbotron top-space">
		<div class="container">
			
			<h3 class="text-center thin">Expertise</h3>
			
			<div class="row">
			@forelse($expertises as $expertise)
				<h1></h1>
				<div class="col-md-4 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="{{$expertise->icone}}"></i>{{$expertise->titulo}}</h4></div>
					<div class="h-body text-center">
						<p>
						{{$expertise->descricao}}
						</p>
					</div>
				</div>
			@empty
				<p>Nenhuma expertise cadastrada!</p>
			@endforelse
			</div> <!-- /row  -->
		
		</div>
	</div>
	<!-- /Highlights -->

	<!-- Social links. @TODO: replace by link/instructions in template -->
	<section id="social">
		<div class="container">
			<div class="wrapper clearfix">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
				</div>
				<!-- AddThis Button END -->
			</div>
		</div>
	</section>
	<!-- /social links -->

	@include('partial_view.footer')

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>