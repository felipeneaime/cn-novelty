<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
@include('layout.app')
</head>

<body class="home">
	@include('partial_view.menu-superior')

	<div class="container">
		<div class="row">
			<br/><br/><br/><br/><br/>
			<h2 class="text-center">Detalhe de Portifólio</h2>
			<hr/>
		</div>
		<div class="row">
			
			<!-- Article main content -->
			<article class="col-sm-12 maincontent">
				<header class="page-header">
					<h1 class="page-title">{{$portifolio->titulo}}</h1>
				</header>
				<h3>Sobre a Empresa</h3>
				<p style="text-align:justify">
					<img src="{{$portifolio->imagem}}" alt="" class="img-rounded pull-right" width="300" >
					{{$portifolio->sobre_empresa}}
				</p>

				<h3>Descrição do serviço</h3>
				<p style="text-align:justify">
				{{$portifolio->descricao}}
				</p>

				<h3>Galeria de Fotos:</h3>


				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">

					<div class="item active">
						<img src="{{ url('site_cnnovelty/public/storage/img-portifolio/'.$portifolio->id.'/galeria/'.$img_inicial) }}" style="width:100%;">
						<div class="carousel-caption">
						</div>
					</div>
					@foreach($imgs as $img)
					<div class="item">
						<img src="{{ url('site_cnnovelty/public/storage/img-portifolio/'.$portifolio->id.'/galeria/'.$img) }}" style="width:100%;">
						<div class="carousel-caption">
						</div>
					</div>
					@endforeach
					</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="sr-only">Next</span>
				</a>

				</div>
			</article>
			<!-- /Article -->
		</div>
	</div>


	<!-- Social links. @TODO: replace by link/instructions in template -->
	<section id="social">
		<div class="container">
			<div class="wrapper clearfix">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
				</div>
				<!-- AddThis Button END -->
			</div>
		</div>
	</section>
	<!-- /social links -->

	@include('partial_view.footer')

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
	<script>

	</script>
</body>
</html>