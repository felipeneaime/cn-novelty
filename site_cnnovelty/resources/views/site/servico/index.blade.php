<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
@include('layout.app')
</head>

<body class="home">
	@include('partial_view.menu-superior')

	<div class="container">
		<div class="row">
			<br/><br/><br/><br/><br/>
			<h2 class="text-center">Serviços</h2>
			<hr/>
		</div>
		<div class="row">
		@forelse($servicos as $servico)
			<div class="col-sm-6 col-md-3">
				<div class="thumbnail">
					<h4 style="text-align:center">
						{{$servico->titulo}}
					</h4>
					<img src="{{$servico->imagem}}" alt="...">
					<a href="{{ route('detalheservico.site', $servico->id)}}" class="btn btn-primary col-xs-12" role="button">Visualizar</a>
					<div class="clearfix" style="text-align:justify"></div>
				</div>
			</div>
			@empty
				<p>Nenhum serviço cadastrado!</p>
			@endforelse
		</div>
	</div>


	<!-- Social links. @TODO: replace by link/instructions in template -->
	<section id="social">
		<div class="container">
			<div class="wrapper clearfix">
				<!-- AddThis Button BEGIN -->
				<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				<a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
				</div>
				<!-- AddThis Button END -->
			</div>
		</div>
	</section>
	<!-- /social links -->

	@include('partial_view.footer')

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="assets/js/headroom.min.js"></script>
	<script src="assets/js/jQuery.headroom.min.js"></script>
	<script src="assets/js/template.js"></script>
</body>
</html>