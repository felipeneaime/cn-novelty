<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('/', 'Site\SiteController@index')->name('index.site');

$this->get('/portifolio', 'Site\SiteController@portifolio')->name('portifolio.site');
$this->get('/portifolio/{id}/detalhe', 'Site\SiteController@detalhePortifolio')->name('detalheportifolio.site');

$this->get('/servicos', 'Site\SiteController@servico')->name('servico.site');
$this->get('/servicos/{id}/detalhe', 'Site\SiteController@detalheServico')->name('detalheservico.site');


$this->group(['middleware' => ['auth'], 'namespace' => 'Administracao'], function(){
    $this->get('administracao', 'AdministracaoController@index');
    $this->get('administracao/profile/edit-profile', 'UserController@profile')->name('profile');
    $this->post('administracao/profile/atualizar-profile', 'UserController@profileUpdate')->name('profile.update');
});

$this->group(['middleware' => ['auth'], 'namespace' => 'Administracao\pages'], function(){
    $this->get('administracao/expertise', 'ExpertiseController@index')->name('list.expertise');
    $this->get('administracao/expertise/{id}/edit', 'ExpertiseController@editExpertise');
    $this->post('administracao/expertise/{id}/update', 'ExpertiseController@updateExpertise')->name('update.expertise');
    
    $this->get('administracao/portifolio', 'PortifolioController@index')->name('list.portifolio');
    $this->get('administracao/portifolio/create', 'PortifolioController@create')->name('create.portifolio');
    $this->post('administracao/portifolio/save', 'PortifolioController@save')->name('save.portifolio');
    $this->get('administracao/portifolio/{id}/edit', 'PortifolioController@edit')->name('edit.portifolio');
    $this->post('administracao/portifolio/{id}/update', 'PortifolioController@update')->name('update.portifolio');

    $this->get('administracao/servicos', 'ServicoController@index')->name('list.servicos');
    $this->get('administracao/servicos/create', 'ServicoController@create')->name('create.servicos');
    $this->post('administracao/servicos/save', 'ServicoController@save')->name('save.servicos');
    $this->get('administracao/servicos/{id}/edit', 'ServicoController@edit')->name('edit.servicos');
    $this->post('administracao/servicos/{id}/update', 'ServicoController@update')->name('update.servicos');

    $this->get('administracao/categoria-produtos', 'CategoriaController@index')->name('list.categoria-produto');
    $this->get('administracao/categoria-produtos/create', 'CategoriaController@create')->name('create.categoria-produto');
    $this->post('administracao/categoria-produtos/save', 'CategoriaController@save')->name('save.categoria-produto');
    $this->get('administracao/categoria-produtos/{id}/edit', 'CategoriaController@edit')->name('edit.categoria-produto');
    $this->post('administracao/categoria-produtos/{id}/update', 'CategoriaController@update')->name('update.categoria-produto');

    $this->get('administracao/produtos', 'ProdutoController@index')->name('list.produto');
    $this->get('administracao/produtos/create', 'ProdutoController@create')->name('create.produto');
    $this->post('administracao/produtos/save', 'ProdutoController@save')->name('save.produto');
    $this->get('administracao/produtos/{id}/edit', 'ProdutoController@edit')->name('edit.produto');
    $this->post('administracao/produtos/{id}/update', 'ProdutoController@update')->name('update.produto');

    $this->get('administracao/vendas', 'OrcamentoController@index')->name('list.orcamento');
    $this->get('administracao/vendas/create', 'OrcamentoController@create')->name('create.orcamento');
    $this->get('administracao/vendas/view/{id}', 'OrcamentoController@viewOrcamentos')->name('view.orcamento');
    $this->get('administracao/vendas/view/{id}/pdfOrcamento','OrcamentoController@pdfOrcamentos')->name('pdf.orcamento');
    $this->post('administracao/vendas/save', 'OrcamentoController@save')->name('save.orcamento');
    $this->get('administracao/vendas/{id}/edit', 'OrcamentoController@edit')->name('edit.orcamento');
    $this->post('administracao/vendas/{id}/update', 'OrcamentoController@update')->name('update.orcamento');

    $this->get('administracao/vendas/requisicao-saidas', 'RequisicaoSaidasController@index')->name('list.saidas');
    $this->get('administracao/vendas/create-requisicao-saidas', 'RequisicaoSaidasController@create')->name('create.requisicao-saidas');
    $this->post('administracao/vendas/save-requisicao', 'RequisicaoSaidasController@save')->name('save.requisicao');
    
    $this->get('administracao/estoque/entrada-produtos', 'EstoqueController@entrada')->name('entrada.estoque');
    $this->get('administracao/estoque/saida-produtos', 'EstoqueController@saida')->name('saida.estoque');
    $this->post('administracao/estoque/saveEntrada', 'EstoqueController@saveEntrada')->name('saveEntrada.estoque');
    $this->post('administracao/estoque/saveSaida', 'EstoqueController@saveSaida')->name('saveSaida.estoque');
    $this->get('administracao/estoque/list-status', 'EstoqueController@listStatus')->name('listStatus.produto');
    $this->get('administracao/estoque/requisicoes-saidas', 'EstoqueController@requisicoesSaidas')->name('requisicoes.saidas');
    $this->get('administracao/estoque/list-status/status/{id}', 'EstoqueController@statusProduto')->name('visualizarHistorico.produto');
    $this->get('administracao/estoque/view/{id}', 'RequisicaoSaidasController@viewRequisicaoSaida')->name('view.requisicao-saida');
    $this->get('administracao/vendas/view/{id}/pdfRequisicao','RequisicaoSaidasController@pdfRequisicao')->name('pdf.requisicao');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
